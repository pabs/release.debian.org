To: debian-devel-announce@lists.debian.org
Subject: Release update: editorial changes to the testing propagation scripts

Hello world,

Anthony Towns has committed a minor change to the britney script which
manages updates of packages to testing, and as a result packages are no
longer being accepted into testing without hand-approval by a member of
the release team.

Wait, that didn't come out quite right.  Let's try again.

  Sarge is now frozen!   Wheeeeeee!!!

Thanks are due to everyone who has helped get us to this point:  in
particular our ftpmasters, Anthony Towns, James Troup, and Ryan Murray,
for their continued dedication which has made it possible for mortals to
wrangle behemoths such as the 9,000-package sarge; our co-wranglers, the
release assistants Andreas Barth, Frank Lichtenheld, and Joey Hess; and
you, gentle maintainer, for your support and patience.

For those maintainers whose packages were unprepared for a freeze at
this moment (the process has, after all, been a long one), you still
have one last opportunity to get things into shape if there are any
remaining important problems. Read on.


Now to explain what, exactly, we mean by "freeze".  The base freeze
upload policy of uploading changes in through unstable if you can,
and testing-proposed-updates if you must, has worked well (or so is the
subjective opinion of the release team), so we plan to continue to apply
the same policy for the freeze of the rest of the archive.

This means that, for all packages that still need to be updated for
sarge, the rules are as follows:

  - If your package needs to be updated for sarge, and the version in
    unstable doesn't contain extraneous changes (e.g, the version is the
    same between testing and unstable), please upload your fix to
    unstable and contact debian-release@lists.debian.org.

  - If the version in unstable already includes significant changes not
    related to the bug to be fixed, contact debian-release about
    uploading to testing-proposed-updates.  Changed dependencies, new
    upstream versions, changed library names, and completely rewriting
    the packaging are "significant changes".  So are lots of other
    things.

  - If the version in unstable won't reach testing because of new
    library dependencies, contact debian-release about uploading to
    testing-proposed-updates.

  - If in doubt, contact debian-release first.

  - In all cases, when preparing an upload please do not make changes to
    the package that are not related to fixing the bugs in question.
    Doing so makes it more time consuming for the release team to review
    and approve such requests, delaying the release.  It also delays the
    fix for your package, because you will be asked to reupload.

  - When contacting the release team, please explain why you are
    requesting an update.  Bug numbers are a must.  The more we can
    figure out from your first email and your changelog (if any), the
    more quickly we can get your update in.

  - If you have a package that needs updating, *please* don't forget to
    contact us.  *Don't expect us to find out about it on our own*.
    Putting a comment in the changelog is not contacting the release
    team. :)  (This has happened at least a couple of times during the
    base freeze; it's not a very good way of getting your package
    approved quickly.)


Now, so as not to have everyone contact us at once about packages we
know we won't approve, here are the guidelines for changes that will be
accepted into testing during the freeze:

  - fixes for release critical bugs (i.e., bugs of severity critical,
    grave, and serious) in all packages

  - fixes for severity: important bugs in packages of priority: optional
    or extra, only when this can be done via unstable

  - translation updates

  - documentation fixes

See <http://lists.debian.org/debian-devel-announce/2004/08/msg00001.html>
for comparison.

As always, it is the release team's goal to get as much good software
into sarge as possible.  However, a freeze does not mean that your
package is ensured a spot in the release.  Please continue to stay on
top of release-critical bugs in packages that you maintain; RC bugs in
optional or extra packages that remain unfixed after a week will still
be grounds for removal from testing, just as they have been up to this
point.

Please also note that since many updates (hopefully, the vast majority)
will still be going in through unstable, major changes in unstable right
now can disrupt efforts to get RC bugs fixed.  We don't ask you not to
make changes in unstable, but we do ask that you be aware of the effects
your changes can have -- especially if you maintain a library -- and to
feel free to continue making use of experimental where appropriate.
Note once again that you can stage NEW uploads in experimental to avoid
disruption in unstable.


Timeline
--------

Now, let's resurrect that earlier freeze timeline, and make a few
changes to it based on the great improvements in sarge since we last
looked at the question:

  3 May 2005
  ~85 RC bugs
  testing-proposed-updates, testing-security working for all architectures
  Official security support for sarge begins
  Freeze time!

Joey Schulze of the security team has given the thumbs-up for official
security support for sarge as of the time of the freeze.  Which is now.

With security support in place, adventurous users can begin testing the
upgrade path from woody to sarge.  Their feedback will be used for
further bugfixing of packages, and to start preparing release notes.
Currently, this is tempered by known bug #278495, which affects perl
when upgrading on a sizable percentage of systems.  We hope this bug
will be addressed soon, so that we can start getting good upgrade
reports in ASAP.


  5-8 May 2005
  ~70-60 RC bugs
  Bug Squashing Party

Frank Lichtenheld has, with prescient and uncanny timing, announced a
BSP for this coming weekend[1].  Now that we have a relatively fixed
number of release-critical bugs in sarge, we hope to quickly eliminate
the remaining RC bugs.  We need your help to do that, though, so
volunteers for squashing bugs and processing upgrade reports is
appreciated.  If you are interested in helping to process upgrade
reports, please subscribe to debian-testing@lists.debian.org and
contact debian-release.


  18 May 2005
  ~15 RC bugs
  d-i final if needed

As before, being able to hold to this schedule depends heavily on a
steadily dropping RC bug count, so if that isn't happening, the timeline
will have to be tweaked accordingly.  Security bugs will, however, not
figure into this count for the most part because they can and will be
fixed post-release.

Experience shows that d-i releases take a bit longer to roll out than
this timeline is really allowing for, so the plan is to release sarge
with d-i RC3.  However, there has been some discussion of less intrusive
per-arch uploads of the installer, to take care of some specific kernel
bugs; if this happens, it would be around this date.

  27 May 2005
  0 RC bugs

Any remaining release-critical bugs will be fixed through uploads to
testing-proposed-updates or by removals from sarge.

With a final cut of the installer in the bag and the effective RC count
down to zero, it's time to finalize the installation manual and release
notes and to create official CD images.


  30 May 2005
  Release

And if everything goes well, we'll be ready to release at the end of the
month.


We'll continue to post updates as the freeze goes on.  For now, please
concentrate on fixing the last few issues, and let's see if we can stick
to this schedule!

Cheers,
-- 
Steve Langasek and Colin Watson
Debian Release Managers
http://release.debian.org/

[1] http://lists.debian.org/debian-devel-announce/2005/05/msg00000.html
