To: debian-devel-announce@lists.debian.org
Subject: Release candidate architecture requalification results; amd64 is RC

Happy Holidays from the release team!

The two-month architecture requalification period set by the release
team in September[1] is over.  Although no updates were sent to d-d-a
during this time about the progress of the requalifications, we had a
very fruitful discussion with porters during our arranged IRC
session[2], and porters have continued to keep us apprised of their
status via wiki.debian.org and privately, with the result that
http://release.debian.org/etch_arch_qualify.html accurately reflects the
status of our ports today vis à vis the qualification standards.  I can
honestly say that I'm quite pleased with the outcome:  rather than our
projection this spring of four architectures making the cut for etch,
there are at this point eight architectures (counting amd64) that have
met the stated requirements, and the positive impact on the release
process with respect to these architectures has been noticeable.  Kudos
to all of our porters for their continued work in keeping us on-track
for a timely etch release.

That does leave four ports released with sarge which are not considered
release candidates for etch at this time.  Those ports are arm, m68k,
s390, and sparc.  We'll go into the details of which criteria are not
currently met by each architecture further on, but first, to the porters
of these architectures, let me assure you that this is not the final
word on the subject.  Although this change in status means that bugs
specific to these architectures should be treated as important instead
of release-critical, and the testing scripts will accept updates of
packages in testing even when one of these architectures is not keeping
up, non-release architectures will have the option in the future to be
reconsidered at two-month intervals -- giving each of these
architectures a number of opportunities to become release candidates
again before the etch release.

And to the porters of the other eight architectures (alpha, amd64, hppa,
i386, ia64, mips, mipsel, and powerpc):  let me caution you that this is
not the final word on the subject. :-)  You can all take a deep breath
now if you like, but please don't treat this requalification as a reason
to ignore the port's health from now until release.  The issues
identified as release criteria are genuinely important, and we are
counting on porters to help us keep up the pace for etch.

As an aside, note that one of the criteria, buildd redundancy, has been
waived for all architectures for this round.  This is not because it's
unimportant, but because we were starting from a point where no
architectures *had* such redundancy and so the only architecture that
would qualify today is powerpc.  We look forward to the point when it is
possible to expect such redundancy to be in place, but the day-to-day
impact on release preparation is negligible except in case of a failure
of the *non*-redundant buildd.

One more item of general interest before we get into specifics.  As
AMD64 has met the arch requalification requirements, and amd64 inclusion
in the Debian archive is a release blocker for etch, amd64-specific bugs
should be considered release-critical under the same terms as for the
other seven architectures.  If your package builds but fails to work at
all on amd64, this *is* a grave bug, just as it is for other
architectures.  (However, since there are currently no previously-built
amd64 packages in the Debian archive proper, build failures on amd64 are
not yet RC.)

So, here's what's not quite up to snuff about the four ports being
dropped from the list.

ARM
---
ARM has been borderline on a lot of the criteria, which we were willing
to consider waiving; but there are two points in particular which we
aren't willing to waive.  First is that the porting machine for arm,
debussy.debian.org, has been off-line now for a number of months, and no
candidate replacement machine has yet been made available.  If
developers are going to be asked to support an architecture, they must
all have access to hardware that they can use for debugging problems.
In the meantime, developers who need access to an arm machine for
debugging can follow the directions at [3], this just isn't enough to
satisfy the release requirements.

The second point is that, to our understanding, there is no upstream
support for binutils.  Although most of the other core packages are
supported upstream, and there is no explicit arm-specific problem we
know of with binutils today, we can't be sure that this will be true for
the length of the etch release cycle without someone doing the necessary
work upstream in advance of Debian packaging changes.  Given that we
have seen major toolchain issues this year on architectures that do have
upstream support (including numerous g++-4.0 ICEs on arm, and lingering
java bootstrapping issues), we do need to have confidence that Debian
unstable is not the first line of defense against toolchain regressions
for a port.

With those two issues, combined with this port being borderline on
buildd count and ability to keep up with unstable (dipping briefly below
94% up-to-date again with the uploads for the second C++ transition[4]),
we believe it's best to let this port do a little more work to sort
things out on its own before being yoked again to the release.


Motorola 68k
------------
Another port that's been hard-hit by toolchain updates has been m68k.
Although some g++-4.0 problems that were common to arm, hppa, and m68k
have been dealt with or patched around, we are still seeing other
m68k-specific failures; so while there is upstream toolchain support, it
seems insufficient to allow m68k to keep up with unstable today.
Between these issues and the known performance gap between m68k and
other architectures, m68k has not reached the target 98% up-to-date mark
at any point in the past three months, and has spent most of that time
below the historical 95% cut-off point.

Also, although we've been willing to consider waiving the buildd count
requirement for m68k, there are 18 m68k buildds registered with
wanna-build today, and this seems to be barely enough to make headway
against the list of packages needing built[5].  This may in part be due
to the fact that six of these buildds also appear to be down right now;
which supports the assertion that per-buildd power, and not just
redundancy, is a factor in an architecture's ability to keep up.

There are further issues with kernel support on m68k, as some of the
subarchitectures are not yet supported stably under 2.6 kernels.  Work
is in progress, and we look forward to hearing from the m68k porters
again once this is farther along.


S/390
-----
The single problem of the s390 port today appears to be a lack of
developer interest.  Although Bastian Blank and Frans Pop have done a
fine job of keeping this port running to date, it isn't really fair to
expose users of a stable release to such a high "vacationing-in-Tahiti"
factor.  There is also concern that the available manpower may not be
enough to get the etch installer in shape in a reasonable timeframe.

If you're a developer and want to see s390 shipped with etch, now's the
time to sign up and pitch in!


Sun SPARC
---------
Initially, it seemed like sparc ought to have the easiest time of the
four to get requalified.  Despite misgivings earlier this year about
upstream support, sparc is generally doing well; since the sarge
release, two new sparc buildds have been brought on-line.
Unfortunately, in the same period two buildds have gone *off-line*,
including vore.debian.org which was the designated porter machine; and
conversations with DSA revealed the presence of persistent kernel
problems affecting the stability of the remaining buildds.  It is taking
some time to pin down this problem since it only shows up under load,
but the porters are working their way through the bug step by step.  In
the meantime, even if this bug might not affect all sparc hardware,
having it affect all of our build daemons is certainly a showstopper, so 
sparc will be removed from consideration until we no longer have to
worry about OpenOffice builds (or other intensive package builds)
crashing the buildd machines.


Honorable mentions: HPPA, Alpha
-----------------------
For a variety of reasons, this mail is being sent out over a month after
it was originally due.  In that time, a couple of other ports have shown
signs of lagging in key areas; so lest people think we're playing
favorites, here's a few words about that.

Although hppa has only briefly dropped below the 95% cutoff over the
past three months, recent toolchain bugs have been preventing this
architecture from building KDE for the latest ABI transition.  While
this has not yet become a blocker for the transition, it is a definite
concern for the release team, and the porters have been informed of the
potential consequences for the architecture's release status.  Since
hppa toolchain support seems to be incompletely integrated upstream, the
porters will have an additional burden to carry in keeping this port
running in sync with the Debian sources.

Also, for the past week the alpha autobuilder has been off-line while it
is being physically relocated, leading to a conspicuous drop in the
port's up-to-dateness and reminding us once again the importance of
buildd redundancy.  Since this is expected to be a temporary problem,
and we are not yet counting lack of buildd redundancy against ports,
this is not yet a reason to disqualify alpha from the etch release.

--
Steve Langasek                                     [vorlon@debian.org]
Debian Release Team

[1] http://lists.debian.org/debian-devel-announce/2005/09/msg00012.html
[2] http://lists.debian.org/debian-devel-announce/2005/10/msg00000.html
[3] http://wiki.debian.org/armEtchReleaseRecertification 
[4] http://buildd.debian.org/stats/graph2-quarter-big.png
[5] http://people.debian.org/~igloo/needs-build-graph/index.php?arches=&days=60
