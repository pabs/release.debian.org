Rebuilding in testing-proposed-updates for a transition
=======================================================


When packages involved in a transition are stuck in unstable (and
non-smooth-updatable), it might can help to rebuild them in
testing-proposed-updates. That usually needs some build-deps in tpu first.



Get build-deps from unstable into tpu
-------------------------------------

Get the info from unstable in the correct format. Note that unstable will
probably contain old sourced and binaries, so you should only get the version
you want.

dak ls -f control-suite -s unstable -S perl | grep 5.30.0-6 | sort > perl-for-tpu

Check that this is actually what you want.

If tpu isn't empty, add the output of
dak control-suite -l testing-proposed-updates

as release@respighi:

cd ~release/sets/testing-proposed-updates/
cp (some dir)/perl-for-tpu 20191011.18:30
ln -s 20191011.1830 current
./set_tpu


If you needs some additional build-deps from unstable, it might be some work
to find out which ones are needed. In the perl case, the build logs from
unstable might give a list of binNMUs that are needed. Note that dak doesn't
accept versions into tpu that are not newer than testing or unstable, so if
you copy binNMUs, exclude the source.


Schedule binNMUs in testing
---------------------------

This works just like in unstable, but with the added 'testing':

wb nmu nginx . ANY . testing . -m "build against perl 5.30.0" --extra-depends "perl-base (>= 5.30)"

You will also need an unblock hint to allow the binNMUs into testing:

unblock nginx/1.14.2-3

Note that build in testing will only begin after dinstall, so you'll need to
wait for them.


If testing and unstable have the same source version, but you still want to do
a rebuild in tpu, because the binNMU from unstable won't be able to migrate,
you need to make sure that the binNMU version in unstable is higher than the
one in tpu, and you should not reuse version.

libguestfs was at +b2 in unstable:

Rebuild with a higher version in unstable (only on release archs):

wb nmu 10 libguestfs . amd64 i386 armel armhf arm64 mipsel mips64el ppc64el s390x . -m "build against perl 5.30.0 with higher binNMU number to allow rebuild in tpu" --extra-depends "perl-base (>= 5.30)"

Rebuild with the next version in testing (but lower than unstable):

wb nmu 3 libguestfs . ANY . testing . -m "build against perl 5.30.0" --extra-depends "perl-base (>= 5.30)"

